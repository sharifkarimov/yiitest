function allowDrop(ev) {
    ev.preventDefault(); 
  }
  function dragStart(ev) {
    
    ev.dataTransfer.setData("text/plain", ev.target.id);
  }
  function dropIt(ev) {
    ev.preventDefault();  
    let sourceId = ev.dataTransfer.getData("text/plain"); //card idsi
    let sourceIdEl=document.getElementById(sourceId);
    
    let sourceIdParentEl=sourceIdEl.parentElement;
    // ev.target.id  bu card joylanadigan content idsi
    let targetEl=document.getElementById(ev.target.id)
    let targetParentEl=targetEl.parentElement;
  	
    let parentDataStatus = $('#'+ev.target.id).attr('data-status');
    
    let childDataId = $('#'+sourceId).attr('data-id');
   
   	ajaxChangeStatus(parentDataStatus,childDataId);
    
    if (targetParentEl.id!==sourceIdParentEl.id){
	// If the source and destination have the same 
        // className (card), then we risk dropping a Card in to a Card
        // That may be a cool feature, but not for us!
        if (targetEl.className === sourceIdEl.className ){
          // Append to parent Object (list), not to a 
          // Card in the list
          // This is in case you drag and drop a Card on top 
          // of a Card in a different list
           targetParentEl.appendChild(sourceIdEl);
         
        }else{
            // listga qo'shish
            targetEl.appendChild(sourceIdEl);  
        }
    }else{
        // Same list. Swap the text of the two cards
        // Just like swapping the values in two variables
      
        // Temporary holder of the destination Object
        let holder=targetEl;
        // The text of the destination Object. 
        // We are really just moving the text, not the Card
        let holderText=holder.textContent;
        // Replace the destination Objects text with the sources text
        targetEl.textContent=sourceIdEl.textContent;
        // Replace the sources text with the original destinations
        sourceIdEl.textContent=holderText;
        holderText='';
	}
 }

 function ajaxChangeStatus(parentDataStatus, childId){
 	$.ajax({
 		method:"GET",
 		url: "ajax-change-status",
 		data:{
 			status: parentDataStatus,
 			id: childId
 		}
 	}).done(function(data){

 	});
 }
$(document).ready(function(){
	$('.add-form').click(function(){
 	form = '<form id="'+$(this).attr('data-form-id')+'" method="POST" action="add-task" class="" style="display: flex" role="form">'+
				'<textarea name="text" class="form-control col-md-8"></textarea>'+
				'<a href="javascript:addTask('+$(this).attr('data-form-id')+', '+$(this).attr('data-status')+')" class="col-md-2 btn btn-info add-btn" data-form-id="'+$(this).attr('data-form-id')+'"><i class="fa fa-check"></i></a>'+
				'<a data_add_btn="'+$(this).attr('id')+'" href="javascript:removeBtn('+$(this).attr('id')+','+$(this).attr('data-form-id')+')" type="button" dataFormId="add_form_id" class="col-md-2 btn btn-danger">X</a>'+
			'</form>';
	$($(this).attr('data-div-id')).append(form);
	$(this).hide();
 });
});

function removeBtn(btn_id, form_id){
	$('#'+btn_id.id).show();
 	$('#'+form_id.id).remove();
}

function addTask(e,status){
	form = $('#'+e.id);
	text = form.find('textarea').val();

	$.ajax({
		type:'POST',
		url: 'add-task',

		data:{
			text:text,
			status:status
		}
	}).done(function(data){
		card = '<div class="border hover col-md-12 p-5  m-t-10" id="card'+data.id+'" data-id="'+data.id+'" draggable="true" ondragstart="dragStart(event)">'+
					'<div class="col-md-10">'+
						'<p id="card-text-'+data.id+'">'+data.text+'</p>'+
					'</div>'+
						'<div class="col-md-2">'+
							'<a href="javascript:editModal('+data.id+')"><i class="fa fa-pencil modal-btn" data-id="'+data.id+'" ></i></a>'+
							'<a href="javascript:removeCard('+data.id+')"><i class="fa fa-trash delete-card" data-id="'+data.id+'" ></i></a>'+
						'</div>'+
				'</div>';
		$('#list'+data.status).append(card);
		$('#add_btn_'+data.status).show();
 		$('#form_'+data.status).remove();
	});
}


function editModal(id)
{
	$('.btn-modal').click();
	$('#card-id-modal-input').val(id);
	$('#card-text-modal-input').val($('#card-text-'+id).text());
}

function removeCard(id)
{
	$.ajax({
		type:'get',
		url: 'task-delete',
		data:{
			id:id,
		}
	}).done(function(data){
		$('#card'+data.id).remove();
	});
}

$('.save-change-btn').click(function(){
	text = $('#card-text-modal-input').val();
	id = $('#card-id-modal-input').val(); 
	$.ajax({
		type:'POST',
		url: 'add-task',
		data:{
			id:id,
			text:text
		}
	}).done(function(data){
		$('#card-text-'+data.id).text(data.text);
		$('.close').click();
	});
});
