<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\User;
use yii\base\Model;

use Yii;

class Register extends Model 
{
	
	public $id;
    public $username;
    public $password;
    public $name;

	public function rules()
	{
		return [
			['username', 'required'],
			['username', 'unique', 'targetClass' => 'app\models\User', 'message' => 'This username has already been taken.'],
			['password','required'],
			['name', 'required']
		];
	}

	

    public function register()
    {
    	if(!$this->validate()){
    		return null;
    	}

    	$user = new User();
    	$user->username = $this->username;
    	$user->name = $this->name;
    	$user->password = Yii::$app->security->generatePasswordHash($this->password);

    	return $user->save();

    }

}


?>