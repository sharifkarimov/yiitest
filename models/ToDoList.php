<?php

namespace app\models;
use yii\base\Model;
use yii\db\ActiveRecord;
use app\models\User;
use Yii;
class ToDoList extends ActiveRecord
{	
  	public static function tableName() {
  		return 'todolist';
  	}

  	public function rules()
  	{
  		return [
  			['text', 'required'],
  			['user_id', 'required']
  		];
  	}

    public function getStatusText()
    {
        switch ($this->status) {
            case 1:
              return 'New';
            case 2:
              return 'To DO';
            case 3:
              return 'Done';           
          }  
    }

    public function getUser()
    {
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }
}


?>