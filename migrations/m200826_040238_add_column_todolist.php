<?php

use yii\db\Migration;

/**
 * Class m200826_040238_add_column_todolist
 */
class m200826_040238_add_column_todolist extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('todolist', 'status', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200826_040238_add_column_todolist cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200826_040238_add_column_todolist cannot be reverted.\n";

        return false;
    }
    */
}
