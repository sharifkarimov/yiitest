<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m200821_162858_todo_list_table
 */
class m200821_162858_todo_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('todolist',[
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'text' => $this->text(),
            'user_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200821_162858_todo_list_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200821_162858_todo_list_table cannot be reverted.\n";

        return false;
    }
    */
}
