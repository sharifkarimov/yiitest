<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Register;
use app\models\ToDoList;
use yii\web\Request;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionTodolist()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('login');
        }
        $listTask = ToDoList::find()->where(['status'=>1])->all();
        $listToDo = ToDoList::find()->where(['status'=>2])->all();
        $listDone = ToDoList::find()->where(['status'=>3])->all();
       // var_dump($listTask);
        return $this->render('todolist',[
            'listTask' => $listTask,
            'listToDo' => $listToDo,
            'listDone' => $listDone
        ]);        
    }

    public function actionRegister()
    {
        $model = new Register();
        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            
        }

        $model->password = '';

        return $this->render('register',[
            'model' => $model
        ]);
    }

    public function actionAjaxChangeStatus(){
        $request = Yii::$app->request;

        $id = $request->get('id');
        $status = $request->get('status');

        $model =  ToDoList::findOne($id);
        if (Yii::$app->request->isAjax) {
            $request = Yii::$app->request;
            $model->status = $status;
            $model->save();
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'status' => $status,
            'id' => $id
        ];
    }

    public function actionAddTask()
    {   
        $request = Yii::$app->request;
        if($request->post('id')){
            $model = ToDoList::findOne($request->post('id'));
        } else {
            $model = new ToDoList();
        }

        if (Yii::$app->request->isAjax) {
            $model->text = $request->post('text');
            $model->user_id = Yii::$app->user->id;
            $model->status = $request->post('status');
            $model->save();
          
            $data = [
                'id' => $model->id,
                'text' => $model->text,
                'user_id' => $model->user_id,
                'status' => $model->status,
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $data;
    }

    public function actionTaskDelete(){
        $request = Yii::$app->request;

        $id = $request->get('id');

        $model =  ToDoList::findOne($id);
        if (Yii::$app->request->isAjax) {
            $model->delete();
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'id' => $id
        ];
    }
}
