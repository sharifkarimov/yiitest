<?php


use yii\helpers\Html;

$this->title = 'To Do List'
?>
<style>
	.border{
		border: 1px solid;
		border-color: #9d9d9d;
		border-radius: 5px;
		cursor: pointer;
	}
	.hover:hover{
		box-shadow: 0px 1px 8px 0px;
	}
	.p-5{
		padding:5px;
	}
	.p-t-30{
		padding-top: 30px;
	}
	.max-h-50{
		min-height: 50px;
	}
	.m-t-10{
		margin-top: 10px;
	}
	.p-r-0{
		padding-right: 0px;
	}
	.p-l-0{
		padding-left: 0px;
	}
</style>

<div class="site-about">
	<h1>Board</h1>
</div>
	<div class="col-md-12">
		<div class="col-md-4">
			<h4>Topshiriq</h4>
			<div class="border col-md-12 p-5 p-t-30  max-h-50"  data-status="1"  id='list1' ondrop="dropIt(event)" ondragover="allowDrop(event)">
				<?php foreach($listTask as $rowTask): ?>
					<div class="border hover col-md-12 p-5  m-t-10" id='card<?= $rowTask->id ?>' data-id="<?= $rowTask->id ?>" draggable="true" ondragstart="dragStart(event)">
						<div class="col-md-10">
							<p id="card-text-<?= $rowTask->id ?>"><?= $rowTask->text ?></p>
						</div>
						<?php if(Yii::$app->user->id === $rowTask->user_id ): ?>
							<div class="col-md-2">
								<a href="javascript:editModal(<?= $rowTask->id ?>)"><i class="fa fa-pencil modal-btn" data-id="<?= $rowTask->id ?>" ></i></a>
								<a href="javascript:removeCard(<?= $rowTask->id ?>)"><i class="fa fa-trash delete-card" data-id="<?= $rowTask->id ?>" ></i></a>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="col-md-12 p-r-0 p-l-0">
				<div id="div-form-1">
				</div>
				<span id="add_btn_1" class="btn btn-success  add-form col-md-12 p-r-0 p-l-0" data-status="1" data-div-id="#div-form-1" data-form-id="form_1" >Qo'shish</span>
			</div>
		</div>
		<div class="col-md-4" >
			<h4>Jarayonda</h4>
			
			<div class="border col-md-12  p-5 p-t-30 max-h-50"  id='list2' data-status="2"  ondrop="dropIt(event)" ondragover="allowDrop(event)">
				<?php foreach($listToDo as $rowToDo): ?>
					<div class="border hover col-md-12 p-5 m-t-10"  id='card<?= $rowToDo->id ?>' data-id="<?= $rowToDo->id ?>" draggable="true" ondragstart="dragStart(event)">
						<div class="col-md-10">
							<p id="card-text-<?= $rowToDo->id ?>"><?= $rowToDo->text ?></p>
						</div>
						<?php if(Yii::$app->user->id === $rowToDo->user_id ): ?>
							<div class="col-md-2">
								<a href="javascript:editModal(<?= $rowToDo->id ?>)"><i class="fa fa-pencil modal-btn" data-id="<?= $rowToDo->id ?>" ></i></a>
								<a href="javascript:removeCard(<?= $rowToDo->id ?>)"><i class="fa fa-trash delete-card" data-id="<?= $rowToDo->id ?>" ></i></a>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="col-md-12 p-r-0 p-l-0">
				<div id="div-form-2">
				</div>
				<span id="add_btn_2" class="btn btn-success add-form  col-md-12 p-l-0 p-r-0 " data-status="2" data-div-id="#div-form-2" data-form-id="form_2" >Qo'shish</span>
			</div>
		</div>
		<div class="col-md-4">
			<h4>Tayyor</h4>
			<div class="border col-md-12 p-5 p-t-30 max-h-50" id='list3' data-status="3" ondrop="dropIt(event)" ondragover="allowDrop(event)">
				<?php foreach($listDone as $rowDone): ?>
					<div class="border hover col-md-12 p-5 m-t-10"  id='card<?= $rowDone->id ?>' data-id="<?= $rowDone->id ?>" draggable="true" ondragstart="dragStart(event)">
						<div class="col-md-10">
							<p id="card-text-<?= $rowDone->id ?>"><?= $rowDone->text ?></p>
						</div>
						<?php if(Yii::$app->user->id === $rowDone->user_id ): ?>
							<div class="col-md-2">
								<a href="javascript:editModal(<?= $rowDone->id ?>)"><i class="fa fa-pencil modal-btn" data-id="<?= $rowDone->id ?>" ></i></a>
								<a href="javascript:removeCard(<?= $rowDone->id ?>)"><i class="fa fa-trash delete-card" data-id="<?= $rowDone->id ?>" ></i></a>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="col-md-12 p-r-0 p-l-0">
				<div id="div-form-3">
				</div>
				<span id="add_btn_3" class="btn btn-success add-form  col-md-12 p-l-0 p-r-0" data-status="3" data-div-id="#div-form-3" data-form-id="form_3" >Qo'shish</span>
			</div>
		</div>
	</div>

<a class="btn-modal" style="display:none" data-toggle="modal" data-target="#exampleModal"></a>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tahrirlash</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="card-id-modal-input" value="">
      	<textarea name="text" class="form-control" id="card-text-modal-input"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Yopish</button>
        <button type="button" class="btn btn-primary save-change-btn">Saqlash</button>
      </div>
    </div>
  </div>
</div>