<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="site-register">
    <h1><?= Html::encode($this->title) ?></h1>


    <?php 
    	$form = ActiveForm::begin([
    		'id' => 'register-form',
        	'layout' => 'horizontal',
    	]);
    ?>

    <?= $form->field($model, 'name')->textInput() ?>
    
    <?= $form->field($model, 'username')->textInput() ?>
    
    <?= $form->field($model, 'password')->passwordInput() ?>

     <div class="form-group">
            <div class="col-lg-offset-1 col-lg-6">
                <?= Html::submitButton('Register', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
            </div>
        </div>
    
    <?php ActiveForm::end(); ?>
</div>