<?php

use yii\helpers\Html;
?>
 
<diV class="col-md-12">
	<table class="table table-bordered">
		<caption>Tasks</caption>
		<thead>
			<tr>
				<th>Id</th>
				<th>User</th>
				<th>Text</th>
				<th>Status</th>
				<th class="text-right">Control</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($tasks as $task): ?>
			<tr>
				<td><?= $task->id ?></td>
				<td><?= $task->user->name ?></td>
				<td id="text-<?= $task->id ?>"><?= $task->text ?></td>
				<td id="status-<?= $task->id?>"><?= $task->getStatusText() ?></td>
				<td class="text-right" width="15%">
					<a class="btn btn-info edit" data-id="<?= $task->id ?>" data-status="<?= $task->status ?>">Edit</a>
					<?= Html::a('Delete',['tasks/delete', 'id' => $task->id], ['class'=>'btn btn-danger']);?>
					
				</td>
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>
</diV>

<a class="btn-modal" style="display:none" data-toggle="modal" data-target="#exampleModal"></a>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tahrirlash</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="id-modal-input" value="">
      	<textarea name="text" class="form-control" id="text-modal-textarea"></textarea><br>
      	<select name="" id="task-status-select" class="form-control">
      		<option value="1">New</option>
      		<option value="2">To Do</option>
      		<option value="3">Done</option>
      	</select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Yopish</button>
        <button type="button" class="btn btn-primary save">Saqlash</button>
      </div>
    </div>
  </div>
</div>

<?php
$js = <<< JS
	$('.edit').on('click',function(){
		$('#id-modal-input').val($(this).attr('data-id'));
		let text = $('#text-'+$(this).attr('data-id')).text();
		$('#text-modal-textarea').val(text);
		$('#task-status-select').val($(this).attr('data-status')).trigger('change');
		$('.btn-modal').click();
	});

	$('.save').on('click',function(){
		$.ajax({
			method: 'POST',
			url:'/admin/tasks/edit',
			data:{
				id: $('#id-modal-input').val(),
				text: $('#text-modal-textarea').val(),
				status: $('#task-status-select').val(),
			}
		}).done(function(data){
			$('#text-'+data.id).text(data.text);
			$('#status-'+data.id).text(data.status.text);
			$('.close').click()
		});
	});
JS;

$this->registerJs( $js, $position = yii\web\View::POS_READY, $key = null );
?>