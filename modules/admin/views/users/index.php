<?php


?>
 
<diV class="col-md-12">
	<table class="table table-bordered">
		<caption>Users</caption>
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Username</th>
				<th>Role</th>
				<th class="text-right">Control</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($users as $user): ?>
			<tr>
				<td><?= $user->id ?></td>
				<td><?= $user->name ?></td>
				<td><?= $user->username ?></td>
				<td><?= $user->role_id ?></td>
				<td class="text-right" width="15%">
					<a class="btn btn-info">Edit</a>
					<a class="btn btn-danger">Delete</a>
				</td>
			</tr>
			<?php endforeach;?>
		</tbody>
	</table>
</diV>