<?php



namespace  app\modules\admin\components;

use yii\web\Controller;
use Yii;

class AController extends Controller
{	
	public $layout = '@app/modules/admin/views/layouts/main.php';

	public function beforeAction($action)
	{
		if (!Yii::$app->user->isGuest) {
			if(Yii::$app->user->identity->getRole() != 1){
				return $this->goHome();
			}
		} else {
			return $this->goHome();
		}
		return parent::beforeAction($action);
	}
    
}



?>