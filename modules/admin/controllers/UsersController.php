<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\modules\admin\components\AController;

use app\modules\admin\models\Users;

class UsersController extends AController
{
   	  public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


   	public function actions()
   	{
   		return [
   			'error' => [
   				'class' => 'yii\web\ErrorAction',
   			],
   			'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
   		];
   	}

    public function actionIndex()
    {
    	$users = Users::find()->all();
     	return $this->render('index',[
     		'users' => $users
     	]);
    }
}
