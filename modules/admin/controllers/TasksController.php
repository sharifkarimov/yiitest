<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\AController;
use app\models\ToDoList;
use Yii;
use yii\web\Response;

class TasksController extends AController
{
	public function actionIndex()
    {
    	$tasks = ToDoList::find()->all();
    	return $this->render('index',[
    		'tasks' => $tasks
    	]);
    } 

    public function actionEdit()
	{
		$request = Yii::$app->request;
        if($request->post('id')){
            $model = ToDoList::findOne($request->post('id'));
        }
        if (Yii::$app->request->isAjax) {
            $model->text = $request->post('text');

            $model->status = $request->post('status');
            $model->save();
          
            $data = [
                'id' => $model->id,
                'text' => $model->text,
                'status' => [
                	'code' => $model->status,
                	'text' => $model->getStatusText()
                ]
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $data;
	}  

	public function actionDelete($id)
	{
		  $model = ToDoList::findOne($id);

		  $model->delete();

		  return $this->redirect('/admin/tasks');
	}
}
